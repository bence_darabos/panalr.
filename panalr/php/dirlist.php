<?php
			$mydir = '';
			$dirlist = array();
			$content = array();

			/*foreach(glob($mydir.'*', GLOB_ONLYDIR) as $dir) {
				$dir = str_replace($mydir, '', $dir);
				array_push($dirlist, $dir);
			}*/

			$dirlist = findPortalFolders();

			foreach($dirlist as $dir) {
				$skipthis = false;
				$name = $dir;

				/* BLACKLIST ELEMENTS */
				$blacklist = [ 	"PanasonicAero-JS-Engine",
								"panasonicaero-js-engine",
								"sb-emulist",
								"sb-validator",
								"pac-wja-portal-phase1-whitelist",
								"pac-csh-portal-phase1-whitelist"];

				for ($i = 0; $i < count($blacklist); $i++) {
				    if ($dir==$blacklist[$i]){
				    	$skipthis = true;
				    }
				}

				/* RENAMED ELEMENTS */
				if($skipthis == false){
					if($dir == "gia-desktop-phase1")                    $name = "GIA D";
					elseif($dir == "gia-mobile-phase1")                 $name = "GIA M";
					elseif($dir == "pac-jal-portal-jntomicrosite")      $name = "JAL Micro";
					elseif($dir == "panasonic-qfa-portal-en_gb")        $name = "QFA";
					elseif($dir == "pac-panasky-portal-jsengineportal") $name = "TEMPLATE";
					elseif($dir == "pac-panasky-portal-pe")             $name = "PANASKY PE";
					else{
						preg_match('/pac-(.*)-portal-phase(.*)/', $dir, $matches);
						if(is_array($matches) and count($matches)){
							preg_match('/(.*)_(.*)/', $matches[2], $matches2);
							$name = strtoupper($matches[1]);
							if(intval($matches[2])>1) $name .= " P".$matches[2].'';
							if(count($matches2) && $matches2[2]){
								$name .= ' ' . '"'.$matches2[2].'"';
							}
						}
					}
					
					$portalInfo[$dir]['directory'] = $dir;
					$portalInfo[$dir]['name'] = $name;
					array_push($content, [$name, "
						"]);
				}else{
					$skipthis = false;
				}
			}

			/* Sort portal info array by portal name */
			function sortPortalInfoByNameSorter($item1,$item2)
			{
			    if ($item1['name'] == $item2['name']) return 0;
			    return ($item1['name'] < $item2['name']) ? -1 : 1;
			}
			usort($portalInfo,'sortPortalInfoByNameSorter');
			/* Sort portal info array by portal name END*/
		?>