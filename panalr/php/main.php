<?php
require_once('config.php');

function findPortalFolders($startPath='./',$filterPac = false){
	if(!file_exists($startPath)){
		throw new Exception("File '".$startPath."' does not exists.");
	}
	$allFiles = scandir($startPath);
	$portalFolders = array();
	if($filterPac){
		foreach($allFiles as $file){
			preg_match('/pac-(.*)-portal-phase(.*)/', $file, $matches);
			if(is_array($matches) and count($matches)){
				array_push($portalFolders, $file);
			}
		}
	} else{
		foreach($allFiles as $file){
			if(file_exists($startPath . '/'.$file) && is_dir($startPath.$file)  && $file[0] != '.'){
				array_push($portalFolders, $file);
			}
		}
	}
	$invalidFolders = ['..','.'];
	$portalFolders = array_diff($portalFolders,$invalidFolders);
	return $portalFolders;
}

function findPortalLanguagePaths($portalFolder,$withPath=true){
	$portalFolder = $GLOBALS['config']['portalBasePath'] . $portalFolder;
	if(!file_exists($portalFolder)){
		throw new Exception("File '".$portalFolder."' does not exists. @ findPortalLanguagePaths");
	}
	$dir    = './'.$portalFolder;
	$allFiles = scandir($dir);
	$langFolders = array();
	foreach($allFiles as $file){
		preg_match('/^([a-z]{2}_[A-Z]{2})$/', $file, $matches);
		if(is_array($matches) and count($matches)){
			if($withPath)
				array_push($langFolders, $portalFolder.'/'.$file);
			else
				array_push($langFolders, $file);
		}
	}
	return $langFolders;
}

class PortalCodeProcessor{
	var $source = "";
	var $digestedFileName = "";

	function digest($indexhtmlPath){
		if(!file_exists($indexhtmlPath)){
			throw new Exception("File '".$indexhtmlPath."' does not exists.");
		}
		$this->source = file_get_contents($indexhtmlPath);
		$this->digestedFileName = $indexhtmlPath;
	}

	function save($newFilename = "") {
		$this->RUHungry();			
		$fileName = "";
		if($newFilename != "") {
			$fileName = $newFilename;
		} else {
			$fileName = $this->digestedFileName;
		}

		if(!is_writable($fileName))
			throw new Exception("Could not write file:" . $fileName . " due to lack of write permission.");
		file_put_contents($fileName, $this->source);	
	}

	function RUHungry(){
		if($this->source == ""){
			throw new Exception("No source is digested.");
		}
	}

	function toggleDebugEngine($saveImmediately = false){
		$this->RUHungry();

		$re = "/\\<\\!\\-\\-JS\\sENGINE\\sSTART\\-\\-\\>(.*engine_min.*)\\<\\!\\-\\-JS\\sENGINE\\sEND\\-\\-\\>/Us"; 
		preg_match($re, $this->source, $matches);
		if(is_array($matches) && count($matches)){
			///// it's in production mode
			$debugEngineCode= "\n\t\t\t".'<script src="/PanasonicAero-JS-Engine/vendor/js/underscore-min.js" type="text/javascript"></script>'."\n".
			"\t\t\t".'<script src="/PanasonicAero-JS-Engine/vendor/js/require_min_2.0.4.js" type="text/javascript"></script>'."\n".
			"\t\t\t".'<script src="/PanasonicAero-JS-Engine/test/js/AppDebugRunner.js?basepath=../../" type="text/javascript" id="includeHandler"></script>'."\n\t\t";
			$this->source = str_replace ( $matches[1], $debugEngineCode, $this->source);
			$changedTo = 'debug';
		} else{
			///// it's in debug mode
			$re = "/\\<\\!\\-\\-JS\\sENGINE\\sSTART\\-\\-\\>(.*AppDebugRunner.*)\\<\\!\\-\\-JS\\sENGINE\\sEND\\-\\-\\>/Us"; 
			preg_match($re, $this->source, $matches);
			$productionEgineCode = "\n\t\t\t".'<script src="../engine/engine_min.js" type="text/javascript" id="includeHandler"></script>'."\n\t\t";
			$this->source = str_replace ( $matches[1], $productionEgineCode, $this->source);
			$changedTo = 'production';
		}

		//$re = "/\\<\\!\\-\\-JS\\sENGINE\\sSTART\\-\\-\\>(.*)\\<\\!\\-\\-JS\\sENGINE\\sEND\\-\\-\\>/Us"; 
		//preg_match($re, $this->source, $matches);
		
		if($saveImmediately)
			$this->save();
			
		return $changedTo;
	}

	function getEmulatorVersion(){
		$this->RUHungry();
		preg_match('/.*(em\d+)-ifapi-emulator.1uat.com.*/', $this->source, $matches);
		return $matches[1];
	}

	function getEngineVersion(){
		$this->RUHungry();
		$re = "/\\<\\!\\-\\-JS\\sENGINE\\sSTART\\-\\-\\>(.*engine_min.*)\\<\\!\\-\\-JS\\sENGINE\\sEND\\-\\-\\>/Us"; 
		preg_match($re, $this->source, $matches);
		if(is_array($matches) && count($matches)){
			return 'production';
		} else {
			$re = "/\\<\\!\\-\\-JS\\sENGINE\\sSTART\\-\\-\\>(.*AppDebugRunner.*)\\<\\!\\-\\-JS\\sENGINE\\sEND\\-\\-\\>/Us"; 
			preg_match($re, $this->source, $matches);
			if(is_array($matches) && count($matches)){
				return 'debug';
			}
		}
		return 'failed to deteremine';
	}

	function getMediaPlayerSkinVersion(){
		$this->RUHungry();

		$re = "/media-player-skinning-version\\\".*content=\\\"(.*)\\\"/"; 
		preg_match($re, $this->source, $matches);
		if(is_array($matches) && count($matches)){
			return $matches[1];	
		}
		return '';
	}

	function getQaFixVersion(){
		$this->RUHungry();

		$re = "/\\\"qa-fix-version\\\".*content\\=\\\"(\\d+)\\\"/"; 
		preg_match($re, $this->source, $matches);
		if(is_array($matches) && count($matches)){
			return $matches[1];	
		}
		return '';
	}

	function setEmulator($emulator, $saveImmediately = false){
		$this->RUHungry();
		
		$re = '/.*((em\d+)-ifapi-emulator.1uat.com).*/'; 
		preg_match($re, $this->source, $matches);
		$newEmulatorUrl = $emulator . "-ifapi-emulator.1uat.com";
		$this->source = str_replace ( $matches[1], $newEmulatorUrl, $this->source);
		if($saveImmediately)
			$this->save();
	}
}



function getPortalInfo($portalPath){
	try{
		$langs = findPortalLanguagePaths($portalPath);
		/* TODO:some sort of logic should be implemented here to handle multiple emulator and engine versions across the language folders... */
		//foreach ($langs as $langFolder){
		if(is_array($langs) && count($langs)){
			//$portalInfo[$portalFolder]["emulatorVersion"] = getEmulatorVersion($langs[0]."/index.html");
			//$portalInfo[$portalFolder]["engineVersion"] = getEngineVersion($langs[0]."/index.html");
			$cp = new PortalCodeProcessor();
			$cp->digest($langs[0]."/index.html");

			$json = '{';
			$json .= '"data":{';

			$json .= '"emulator":"'.$cp->getEmulatorVersion().'",';
			$json .= '"engineVersion":"'.$cp->getEngineVersion().'",';
			$json .= '"mediaplayerSkinVersion":"'.$cp->getMediaPlayerSkinVersion().'",';
			$json .= '"qaFixVersion":"'.$cp->getQaFixVersion().'"';

			$json .= '}}';
			echo $json;
		} else {
			throw new Exception('No portal recognized at the given path:'.$portalPath);
		}
	} catch (Exception $e){
		echo processExceptionMessage($e);
		return;
	}
		//}
}

function processExceptionMessage($e){
	return '{"result":"failed", "reason":"'.$e->getMessage().'"}';
}

/*   PROCESS REQUESTS    */
if(array_key_exists('toggleDebug',$_GET)){
	try{
		$langs = findPortalLanguagePaths($_GET['toggleDebug']);
		foreach ($langs as $lang){
			$cp = new PortalCodeProcessor();
			$cp->digest($lang."/index.html");
			$result = $cp->toggleDebugEngine($lang."/index.html",true);
		}
	} catch (Exception $e){
		echo processExceptionMessage($e);
		return;
	}
	echo '{"result":"succes","engineType":"'.$result.'"}';
}

if(array_key_exists('setEmulator',$_GET)){
	try{
		$langs = findPortalLanguagePaths($_GET['setEmulator']);
		foreach ($langs as $lang){
			$cp = new PortalCodeProcessor();
			$cp->digest($lang."/index.html");
			$cp->setEmulator($_GET['emulator'],true);
		}
	} catch (Exception $e){
		echo processExceptionMessage($e);
		return;
	}
	echo '{"result":"succes"}';
}

//TODO:I don't like this part at all, I don't have any better idea though...
if(array_key_exists('get',$_GET)){
	$param = $_GET['get'];
	$onlyPACFolders = false;
	if(array_key_exists('paconly',$_GET)){
		if($_GET['paconly']=="true")
			$onlyPACFolders = true;
		else
			$onlyPACFolders = false;
	}

	/* directory list processing */
	if($param == "dirlist"){
		$portalFolders = findPortalFolders($GLOBALS['config']['portalBasePath'],$onlyPACFolders);
		$json = '{';
		$json .= '"data":[';
		foreach ($portalFolders as $dir){
			$json .= '{"dirname":"'.$dir.'"}';
			if ($dir != end($portalFolders)) 
				$json .= ',';
		}
		$json .= ']}';
		echo $json;
	}

	if($param == "api"){
		echo file_get_contents('../docs/api.html');
	}

	if($param == "portalinfo"){
		getPortalInfo($_GET['portal']);
	}
}

?>
