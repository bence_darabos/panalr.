var lastUsed = false;
var caching = false; //Experimental function, TODO: have to update the cache from time to time, also need a button to update manually

Array.prototype.sortBy = function(p) {
	return this.slice(0).sort(function(a,b) {
		return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
	});
}

var dirlist = [];

//INITIALIZE
$(document).ready(function init(){
	//CACHED OR REGULAR LOAD
	if(localStorage){
		if (localStorage.getItem("dirlist") !== null && caching == true) {
			dirlisttemp = localStorage.getItem("dirlist");
			dirlist = JSON.parse(dirlisttemp);
			$('#footer').append("loaced");
			renderDirlist();
		}
		else {
			getAjax("dirlist").then(function(res){
				res = JSON.parse(res);
				dirlist = res.data;
				sortDirlist();
			});
		}
	}
	else {
		getAjax("dirlist").then(function(res){
			res = JSON.parse(res);
			dirlist = res.data;
			sortDirlist();
		});
	}
});

// SEARCH START
$( "#searchfield" ).focus();	

$( "#searchfield" ).keyup(function() {
	var searchterm = $("#searchfield").val();
	searchterm = searchterm.toLowerCase();
	for(var i=0; i < dirlist.length; i++){
		var diralias = dirlist[i].diralias.toLowerCase();
		if(diralias.indexOf(searchterm)==-1){
			$("#"+dirlist[i].dirname).hide();
		}
		else{
			$("#"+dirlist[i].dirname).show();
		}
	}
});
// SEARCH END

//DIRLIST RELATED START
function renderDirlist(){
	var template = $.templates("#buttonTemplate");
	var htmlOutput = template.render(dirlist);
	$("#container").append(htmlOutput);
}

function sortDirlist(){
	$("#container").html("");
	if(lastUsed) addLastUsed();
	var aliaslist=[];
	$.getJSON("panalr/json/blacklist.json", function(result) {
		blacklist=result.blacklist;
		$.getJSON("panalr/json/aliaslist.json", function(res) {
			aliaslist=res.aliaslist;

			for (var i=0; i<dirlist.length; i++){
				var gotalias = false;
				var isblacklisted = false;
				for(var k=0; k<blacklist.length; k++){
					if(dirlist[i].dirname == blacklist[k]){
						dirlist[i].blacklisted = true;					//TODO: remove element from array if it's blacklisted maybe?
						isblacklisted=true;
					}
				}
				if(isblacklisted==false){
					for(var j=0; j<aliaslist.length; j++){
						if(dirlist[i].dirname==aliaslist[j].name){
							dirlist[i].diralias = aliaslist[j].alias;
							gotalias=true;
							break;
						}
					}
					if(gotalias==false){
						var res = dirlist[i].dirname.match(/pac-(.*)-portal-phase(.*)/);
						if(res){                                                            //TODO - refactor
								res[1] = res[1].toUpperCase();
								if (res[2]>1) {dirlist[i].diralias=res[1]+ " P"+ res[2];}
								else{dirlist[i].diralias=res[1];}
						}
					}
				}
				if(!dirlist[i].diralias){
					dirlist[i].diralias = dirlist[i].dirname;
				}
			}

			dirlist = dirlist.sortBy("diralias");
			
			if(caching == true) localStorage.setItem("dirlist", JSON.stringify(dirlist));
			renderDirlist();
		});
	});
}

function addLastUsed(){
	var lastId = localStorage.getItem("panalr_last");
	lastId = JSON.parse(lastId);
	var lastdirlist = [{"dirname":lastId[0],"diralias":lastId[1]}];
	var template = $.templates("#lastUsedTemplate");
	var htmlOutput = template.render(lastdirlist);
	$("#container").prepend(htmlOutput);
}
//DIRLIST RELATED END

//AJAX CALLS START
function getAjax(request){
	var sendUrl = "panalr/php/main.php?get="+request;
	return $.ajax({url: sendUrl});
}

function toggleEngineAjax(request){
	var sendUrl = "panalr/php/main.php?toggleDebug="+request;
	return $.ajax({url: sendUrl});
}

function setEmulatorAjax(dir,emulator){
	var sendUrl = "panalr/php/main.php?setEmulator="+dir+"&emulator="+emulator;
	return $.ajax({url: sendUrl});
}
//AJAX CALLS END

//SETTINGS RELATED START
function loadSettings(dirname, diralias){
	getAjax("portalinfo&portal="+dirname).then(function(res){
		res = JSON.parse(res);
		if(res.result=="failed"){
			$("#error").html(res.reason);
			toggleError();
		}
		else{
			var res;
			res.data.dirname = dirname;
			res.data.diralias = diralias;
			console.log(res);
			if (res.data.mediaplayerSkinVersion == ""){res.data.mediaplayerSkinVersion = "not available"}
			if (res.data.qaFixVersion == ""){res.data.qaFixVersion="not available"}
			var template = $.templates("#settingsTemplate");
			var htmlOutput = template.render(res.data);
			$("#settings").html(htmlOutput);
			
			var emulist=[];
			for (var i=1; i<25; i++){
				emulist.push("em"+i);
			}

			$.each(emulist, function(key, value) {
				$('#emulators')
					.append(
						$("<option></option>").attr("value",key).text(value)
					);
			});
			
			var empos = emulist.indexOf(res.data.emulator);
			$('#emulators option:eq(' + empos + ')').attr('selected', true);

			showSettings();
		}
	});
}

function hideSettings(){
	$("html").removeClass("settings-open");
	$("html").removeClass("error-open");
}

function showSettings(){
	$("html").addClass("settings-open");
	$("html").removeClass("error-open");
}

function toggleError(){

	$("html").toggleClass("error-open");
}
//SETTINGS RELATED END

function clickHandler(id, alias){
	//save the last used
	var last = [id, alias];
	last = JSON.stringify(last);
	localStorage.setItem("panalr_last", last);

	//click counting
	if (localStorage.getItem(id)) {
		localStorage.setItem(id, Number(localStorage.getItem(id)) + 1);
	} else {
		localStorage.setItem(id, 1);
	}
}

function changeEngine(dir, alias){
	toggleEngineAjax(dir).then(function(res){
		res = JSON.parse(res);
		if(res.result=="failed"){
			$("#error").html(res.reason);
			toggleError();
		}
		else{
			hideSettings();
			loadSettings(dir, alias);
		}
	});
}

function changeEmulator(dir, alias){
	var emulator = $('#emulators option:selected').text();
	setEmulatorAjax(dir, emulator).then(function(res){
		res = JSON.parse(res);
		if(res.result=="failed"){
			$("#error").html(res.reason);
			toggleError();
		}
		else{
			hideSettings();
			loadSettings(dir, alias);
		}
	});
}