var express = require("express"); 
var app = express();
var path = require('path');
var fs = require('fs');
var public = __dirname;

function getDirectories(srcpath){
  return fs.readdirSync(srcpath).filter(file => fs.lstatSync(path.join(srcpath, file)).isDirectory())
}

// SERVE MAIN PAGE
app.get('/', function(req, res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.sendFile(path.join(public + "/index.html"));
    // TODO: Move index.html inline to make the project selfcontaining
    // res.writeHead(200,{'Content-Type': 'text/html'});
    // res.write("<html><body><h1>Panalr.</h1></body></html>");
    // res.end();
});

// API CALL
app.get("/api/folderlist", function(req, res)  { 
    var json = getDirectories('./');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json(json);
    console.log("[/api/folderlist]");
});

// START SERVER
app.use('/', express.static(public));

app.listen(8080, function() {  
    console.log("Panalr is running...");
});